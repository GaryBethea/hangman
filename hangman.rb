ALPHABET = ("a".."z").to_a

class HumanPlayer
  attr_reader :already_guessed, :last_guess
  attr_accessor :word_representation
  
  INT_STRINGS = (0..20).to_a.map!(&:to_s)
  
  def initialize 
    @already_guessed = []
    @word_representation = ""
    @last_guess = ""
  end
  
  def pick_secret_word
    puts "Pick a secret word. How many letters does it have?"
    Integer(gets.chomp)
  end
  
  def receive_secret_length(word)
    word.length.times { @word_representation += "_ " }
  end
  
  def guess
    puts word_representation
    puts "What letter will you guess?"
    guessed_letter = gets.chomp.downcase
    
    if already_guessed.include?(guessed_letter)
      puts "No repeats!"
      guess
    end
    
    already_guessed << guessed_letter
    @last_guess = guessed_letter
  end
  
  def check_guess(secret_word, guessed_letter)
    puts "The computer guessed #{guessed_letter}."
    puts "Does that occur in your word? y/n"
    
    yes_or_no = gets.chomp.downcase
    
    return nil if yes_or_no == "n"
    
    puts "At what indices does #{guessed_letter} appear?"
    response = gets.chomp
    
    indices = []
    response.each_char { |char| indices << Integer(char) if INT_STRINGS.include?(char) }
    
    indices
  end
  
  def handle_guess_response(result)
    return if result.nil?
    
    result.each do |occurence|
      @word_representation[(occurence * 2)] = last_guess
    end
  end
  
  def won?
    return false if word_representation.empty?
    
    !word_representation.include?("_")
  end
end


class ComputerPlayer
  attr_accessor :guessing_length, :last_guess, :current_dict, :word_representation, :letter_counts, :already_guessed
  
  DICTIONARY = File.readlines("dictionary.txt").map(&:chomp)
  
  def initialize
    @already_guessed = []
    @guessing_length = 0
    @last_guess = nil
    @word_representation = ""
    @letter_counts = Hash.new(0)
    @current_dict = nil
  end
  
  def pick_secret_word
    DICTIONARY.sample
  end
  
  def receive_secret_length(word)
    @guessing_length = word #for guessing computer, 'word' is already just the length
    word.times { @word_representation << "_" } 
    nil
  end
  
  def guess
    @current_dict = update_dictionary
    
    @letter_counts = update_letter_counts
    best_guess = nil
    
    while best_guess.nil? || @already_guessed.include?(best_guess)
      best_guess = @letter_counts.max.first
      
      #remove top letter if it has already been guessed
      letter_counts.reject do |letter, count|
        [letter, count] == letter_counts.max
      end
    end
    
    @already_guessed << best_guess
    @last_guess = best_guess
  end
  
  def update_letter_counts
    current_dict.each do |entry|
      entry.each_char do |char|
        @letter_counts[char] += 1
      end
    end
    
    @letter_counts
  end
  
  def update_dictionary
    if @last_guess.nil?
      create_sub_dictionary 
    else
      filter_dictionary
    end
  end
  
  def create_sub_dictionary
    DICTIONARY.select { |entry| entry.length == guessing_length }
  end
  
  def filter_dictionary
    current_dict.select { |entry| still_in_play?(entry) }
  end
  
  def still_in_play?(entry)
    in_play = true
    
    word_representation.each_char.with_index do |word_char, index|
      if ALPHABET.include?(word_char) && entry[index] != word_char
        in_play = false # can I return false here? wary of returning in blocks
      end
    end
    
    in_play
  end
  
  def check_guess(secret_word, guessed_letter)
    return nil unless secret_word.include?(guessed_letter)
    
    occurrences = []
    secret_word.length.times do |letter_index|
      occurrences << letter_index if secret_word[letter_index] == guessed_letter
    end
    
    occurrences
  end
  
  def handle_guess_response(result)
    return if result.nil?
    
    result.each do |index|
      
      @word_representation[index] = @last_guess
    end
  end
  
  def won?
    return false if @word_representation.empty?
    
    !@word_representation.include?("_")
  end
end


class Game
  attr_reader :picking_player, :guessing_player
  
  def initialize(picking_player, guessing_player)
    @picking_player = picking_player
    @guessing_player = guessing_player
  end
  
  def play
    secret_word = picking_player.pick_secret_word
    guessing_player.receive_secret_length(secret_word)
    
    until guessing_player.won?
      guessed_letter = guessing_player.guess
      result = picking_player.check_guess(secret_word, guessed_letter)
      guessing_player.handle_guess_response(result)
    end
    
    puts "Game over! The guesser won. The word was '#{secret_word}.'"
    exit
  end
end